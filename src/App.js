import React, {useState } from 'react';
import VideoPlayer from 'react-video-markers';
import "./App.css";
import ErrorBoundary from "../src/components/ErrorBoundary/ErrorBoundary";

 const App = (props) => {
    const [isPlaying, setPlaying] = useState(false);
    const [volume, setVolume] = useState(0.7);
    const [time, setTime] = useState(0);
    return(
        <ErrorBoundary>
        <main className="main">
            <h1>Video Markers</h1>
            <div className="flex-wrapper">
            <button onClick={()=>{
                setTime(300)
            }
            } style={{backgroundColor:'red'}}> Event 1</button>
            <button onClick={()=>{
                setTime(600)
            }
                }style={{backgroundColor:'green'}}> Event 2</button>
            <button onClick={()=>{
                setTime(1000)
            }
            }style={{backgroundColor:'tomato'}}> Event 3</button>
                <button onClick={()=>{
                    setTime(100)
                }
                } style={{backgroundColor:'royalblue'}}> Zone 1</button>
                <button style={{backgroundColor:'purple'}} onClick={()=>{
                    setTime(789)
                }
                }> Zone 2</button>
                <button style={{backgroundColor:'orange'}} onClick={()=>{
                    setTime(1050)
                }
                }> Zone 3</button>
            </div>
    <VideoPlayer
        url="/videos/safety.mp4"
        isPlaying={isPlaying}
        volume={volume}
        onPlay={()=>setPlaying(true)}
        onPause={()=>setPlaying(false)}
        onVolume={(value)=>setVolume(value)}
        timeStart={time}
        markers={[{id:"zone1", time: 100, color: 'royalblue', title: 'Zone 1'},{id:"zone2", time: 789, color: 'purple', title: 'Zone 2'},{id:"zone3", time: 1050, color: 'orange', title: 'Zone 3'},{id:1, time: 300, color: 'red', title: 'Event 1'},{id:2, time: 600, color: 'green', title: 'Event 2'},{id: 3, time: 1000, color: 'tomato', title: 'Event 3'}]}
    />
        </main>
        </ErrorBoundary>
    )
}
export default App;